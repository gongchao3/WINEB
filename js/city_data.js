function loadSecondCities(firstCity) {
	$.get('http://localhost:8080/region/secondCities', {
		firstCity: firstCity
	}, function(data){
		var html1 = "<dd><a href='#'>"
		var html2 = "</a></dd>";
		var html = "";
		for (i=0; i < data.length; i++) {
			html += (html1 + data[i] + html2);
		}
		document.getElementById("shi-select").innerHTML = html;
		$("#select2 dd").click(function() {
			$(this).addClass("selected").siblings().removeClass("selected");
			if ($(this).hasClass("select-all")) {
				$("#selectB").remove();
			} else {
				var copyThisB = $(this).clone();
				if ($("#selectB").length > 0) {
					$("#selectB a").html($(this).text());
				} else {
					$(".select-result dl").append(copyThisB.attr("id", "selectB"));
				}
			}
			$("#selectC").remove();
			loadThirdCities(firstCity, $(this).children()[0].innerText);
		});
	});
}

function loadThirdCities(firstCity, secondCity) {
	$.get("http://localhost:8080/region/thirdCities", {
		firstCity: firstCity,
		secondCity: secondCity
	}, function(data) {
		var html = "";
		var html1 = "<dd><a href='#'>";
		var html2 = "</a></dd>";
		if (data.length == 0) {
			html = '</br>';
		} else {
			for(i=0; i < data.length; i++) {
				html += (html1 + data[i] + html2);
			}
		}
		
		document.getElementById("qu-select").innerHTML = html;
		$("#select3 dd").click(select3click);
	});
}

